# Sahabat Tutor-Simulation Arduino basic with proteus 

## Getting started

Selamat Datang Di Pelatihan Series From Zero To Hero Programmers For The Basics of The Startup Ecosystem
Basic Arduino Simulation - Basic Web Programmer - Basic Python Image Procesing

Basic Web library Codeigniter 4-Basic Spatial Analyst with ArcGis
presented by sahabattutor.com

## About Sahabat Tutor Training Series

Pelatihan ini diperuntukan bagi orang-orang yang ingin belajar dasar-dasar menjadi programmer hebat di masa datang. (note pelatihan ini bersipat basic tidak cocok untuk yang tingkat advance atau programmer lanjutan). Pelatihan dilakukan secara online melalui aplikasi zoom meeting.

Output dari pelatihan ini yaitu diharapkan menjadi dasar yang kuat bagi pemula yang akan belajar programming sehingga dapat peserta diharapkan dapat mengembangkan dasar pengetahuannya untuk dunia programmer. Peserta pelatihan mampu untuk melakukan penyelesaian dari error sehingga mampu belajar dengan cepat dari banyak tutorial yang sudah ada di internet maupun youtube.

## Description

Pelatihan diadakan secara online pada 28 agustus 2021 sampai 18 September 2021

Pelatihan ini terdiri dari beberapa series yang bisa diikuti

1. Pelatihan simulation control otomatic basic simulation Arduino with proteus.
2. Pelatihan basic web programmer with php for backend end bootstrap for frontend , and database mysql
3. Pelatihan Basic programmer python for image processing with opencv library image for data scient
4. Pelatihan Basic Spasial Analiyst with ArcGis
5. Pelatihan basic code for fullstack developer with framework Codeigniter 4

## Installation

1. Download proteus pada link 
   https://drive.google.com/drive/folders/1q_bXGeVNZPbrWsS8O4z5LwuVvKfotoDa?usp=sharing

2. Download program Arduino
   Dapat di download pada Microsoft store atau pada https://www.arduino.cc/en/software 

3. Menambahkan Library baru pada proteus
Buka folder penyimpanan aplikasi proteus, jika install secara default maka lokasi penyimpanan di C:\Program Files (x86)\Labcenter Electronics\Proteus 8 Professional\DATA\LIBRARY
Pindahkan Library proteus berupa file dengan extensi .LIB dan .IDX

4. Menambahkan Library pada arduino

5. Jalankan proteus

## Usage

Souce code pelatihan dasar programmer untuk dapat melakukan basic create , read, update, and delete with php.

## Contributing

thankyou for all contributed
Rahman Subrata as trainer
Tri Abdi Fauzi as trainer
Galang Yusal Farisi as Project manejer

## Authors and acknowledgment

Show your appreciation to those who have contributed to the project.

https://www.theengineeringprojects.com/
https://www.arduino.cc 
Slide Kontrol Otomatic TMB IPB 2019
https://www.ardutech.com 


## More infromation

more information
http://sahabattutor.com/pelatihan
https://play.google.com/store/apps/details?id=id.codepanda.sahabattutor.tutor&hl=en&gl=US

information
Join with sahabattutor as tutor
ipb.link/panduan-pendaftaran-pengajar-st
