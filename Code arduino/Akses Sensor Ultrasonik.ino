#include <Wire.h>
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27, 2,1,0,4,5,6,7,8,POSITIVE);

const int pingPin = 4; // Trigger Pin of Ultrasonic Sensor semen
const int echoPin = 3; // Echo Pin of Ultrasonic Sensor semen
long duration;
float cm;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  lcd.begin(20,4);
  lcd.backlight();
  lcd.setCursor(3,1);
  lcd.print("Selamat Datang ");
  
}

void loop() {
  // put your main code here, to run repeatedly:
  pinMode(pingPin, OUTPUT);
  digitalWrite(pingPin, LOW);
  delayMicroseconds(2);
  digitalWrite(pingPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(pingPin, LOW);
  pinMode(echoPin, INPUT);
  duration = pulseIn(echoPin, HIGH);
  cm = microsecondsToCentimeters(duration);
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print(cm);

  

}
