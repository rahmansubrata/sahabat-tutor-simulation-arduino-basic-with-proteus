#include <Wire.h>
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27, 2,1,0,4,5,6,7,8,POSITIVE);

const int pingPin=4;
const int echoPin=3;
const int relay= 5 ;
long duration;
float cm;

#include <Stepper.h>
const int stepsPerRevolution = 200;
Stepper myStepper(stepsPerRevolution, 8, 9, 10, 11);

#include <Keypad.h>
const byte ROWS = 4;
const byte COLS = 3;
char keys[ROWS][COLS] = {
  {'7','8','9'},
  {'4','5','6'},
  {'1','2','3'},
  {'*','0','#'}
};
byte colPins[COLS] = {13,12, 7}; // pin 2,3,4,5 untuk pin kolom keypad (lihat gambar)
byte rowPins[ROWS] = {6, 2, 1, 0}; // pin 6,7,8,9 untuk pin baris keypad (lihat gambar)
Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  lcd.begin(20,4);
  lcd.backlight();
  lcd.setCursor(0,0);
  lcd.print("Hallo Word");
  delay(1000);
  pinMode(relay,OUTPUT);
  // set the speed at 60 rpm:
  myStepper.setSpeed(60);
  myStepper.step(-1);
}

void loop() {
  int key = keypad.getKey();
  if(key){
  switch(key)
    {
      case '1':
      pinMode(pingPin,OUTPUT);
      digitalWrite(pingPin,LOW);
      delayMicroseconds(2);
      digitalWrite(pingPin,HIGH);
      delayMicroseconds(10);
      digitalWrite(pingPin,LOW);
      pinMode(echoPin,INPUT);
      duration = pulseIn(echoPin, HIGH);
      cm=microsecondsToCentimeters(duration);
      if(cm > 800){
        digitalWrite(relay,HIGH);
        delay(1000);
        digitalWrite(relay,LOW);
        myStepper.step(1);
        }
      lcd.clear();
      lcd.setCursor(0,1);
      lcd.print(cm); 
      lcd.setCursor(6,1);
      lcd.print("CM"); 
      delay(1000);
      break;
    }
  
  // put your main code here, to run repeatedly:
  }
  
}
long microsecondsToCentimeters(long microseconds)
{ 
  return microseconds/29/2;
  }
